#!/bin/bash -e -o xtrace
APPTAINER_OPTS=
IGNORE_MOUNTS="/pnfs"
UNPACKED_IMAGE_DEFAULT="/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-analysis/general/combine-container"
DOCKER_IMAGE_DEFAULT="docker://gitlab-registry.cern.ch/cms-analysis/general/combine-container"
APPTAINER_OPTS_ENV=
COMBINE_VERSION_DEFAULT="latest"
while [ "$#" != 0 ]; do
  case "$1" in
    -h|--help)
      HELP_ARG=""
      echo "Usage: $0 [-h|--help] ${HELP_ARG}[apptainer-options] [--ignore-mount <dir1[,dir2[,...]]>] [--version|-- <command to run>]"
      echo "Environment variable COMBINE_IMAGE can be set to point to either valid docker/apptainer image or unpacked image path"
      exit 0
      ;;
    --ignore-mount) IGNORE_MOUNTS=$(echo $2 | tr ',' ' '); shift; shift ;;
    --version)
      COMBINE_VERSION=$2 ; shift ; shift ;;
    *)
      # FIXME
      APPTAINER_OPTS="${APPTAINER_OPTS} $1"
      shift
      ;;
  esac
done

MOUNT_POINTS=""
if [ "X${APPTAINER_BINDPATH}" != "X" ] ; then MOUNT_POINTS="${APPTAINER_BINDPATH}" ; fi
if [ -d /cvmfs ] ; then
  for repo in cms unpacked grid ; do
    ls /cvmfs/${repo}.cern.ch >/dev/null 2>&1 || true
  done
  MOUNT_POINTS="${MOUNT_POINTS},/cvmfs"
fi
for dir in /eos /afs /work ; do
  if [ -e $dir ] ; then MOUNT_POINTS="${MOUNT_POINTS},${dir}" ; fi
done

if [ "X${COMBINE_VERSION}" = "X" ] ;then
  COMBINE_VERSION=${COMBINE_VERSION_DEFAULT}
fi

# Check if an image location has been specified manually, set default otherwise
if [ "X${COMBINE_IMAGE}" = "X" ] ;then
  UNPACKED_IMAGE=${UNPACKED_IMAGE_DEFAULT}:${COMBINE_VERSION}
fi

# Check if the image is available in unpacked form
if [ ! -d "${UNPACKED_IMAGE}" ] ; then
  echo "CVMFS unpacked image '${UNPACKED_IMAGE}' not available, resorting to image from registry."
  UNPACKED_IMAGE=${DOCKER_IMAGE_DEFAULT}:{COMBINE_VERSION}
fi

if [ -e $UNPACKED_IMAGE ] ; then
  VALID_MOUNT_POINTS=""
  for dir in $(echo $MOUNT_POINTS | tr ',' '\n' | sort | uniq) ; do
    if [ "${IGNORE_MOUNTS}" != "" ] ; then
      hdir=$(echo $dir | sed 's|:.*||')
      if [ $(echo " ${IGNORE_MOUNTS} " | grep " $hdir " | wc -l) -gt 0 ] ; then continue ; fi
    fi
    bind_dir=$(echo $dir | sed 's|.*:||')
    #if [ ! -e ${UNPACKED_IMAGE}/${bind_dir} ] ; then
    VALID_MOUNT_POINTS="${VALID_MOUNT_POINTS},${dir}"
    #fi
  done
  export APPTAINER_BINDPATH=$(echo ${VALID_MOUNT_POINTS} | sed 's|^,||')
fi

APPTAINER_OPTS="${APPTAINER_OPTS} ${APPTAINER_OPTS_ENV}"

echo "Identifying Combine tool related executables and setting alias for them:"
for i in $(bash -c "apptainer exec ${APPTAINER_OPTS} $UNPACKED_IMAGE /cvmfs/cms.cern.ch/cat/combine_alias/alias_script.sh"); do
  BASE_COMMAND=$(echo $i | sed 's!.*/!!')
  echo $BASE_COMMAND $i
  alias $BASE_COMMAND="apptainer -s exec ${APPTAINER_OPTS} $UNPACKED_IMAGE /cvmfs/cms.cern.ch/cat/combine_alias/exec_script.sh $BASE_COMMAND"
done

echo "Combine should now be available."
