#!/bin/bash

cd /home/cmsusr/$CMSSW_VERSION
source /cvmfs/cms.cern.ch/cmsset_default.sh
cmsenv
echo $CMSSW_BASE
for i in $(find -L $CMSSW_BASE/bin/$SCRAM_ARCH -maxdepth 1 -type f -perm -100 -print); do
  echo $i
 #| sed 's!.*/!!'
done;
