# /cvmfs/cms.cern.ch/cat

Files in this area are deployed to `/cvmfs/cms.cern.ch/cat` via
<https://github.com/cms-sw/cmsdist/blob/HEAD/cms-cat.spec>.

## combine

Usage:

```bash
source /cvmfs/cms.cern.ch/cat/combine_env.sh
```

The script will set aliases for executables available in
`$CMSSW_BASE/bin/$SCRAM_ARCH` in the given image.

Furthermore, it will export the variable `APPTAINER_BINDPATH` to make
`/cvmfs`, `/eos`, `/afs`, `/work` available.

Options can be set by exporting environment variables:

- `COMBINE_VERSION`: default is `latest`
- `COMBINE_IMAGE`: default is `/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/cms-analysis/general/combine-container`
